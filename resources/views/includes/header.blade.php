<!doctype html>
<html>
<head>
    <title>Restaurant</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/css/materialize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Oswald:400' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>

    <style>

        .image-size{
            height: 25vw;
        }

        .deactivate-image{
            -webkit-filter: grayscale(100%) !important;
            filter: grayscale(100%) !important;
            width: 15vw !important;
        }

        .active-image{
            -webkit-filter: hue-rotate(90deg) !important;
            /*filter: hue-rotate(90deg) !important;*/
            filter: brightness(160%)!important;
        }

        .icon-right-size{
            height: 20vw;
            width: 1vw !important;
            left: 2vw !important;
            bottom: 8vw !important;
            cursor: pointer;
        }

        .icon-left-size{
            height: 20vw;
            width: 1vw !important;
            left: 45vw !important;
            bottom: 8vw !important;
            top: 20vw !important;
            transform: scaleX(-1);
            cursor: pointer;
        }
        .card .card-image .card-title {
            z-index: 2;
        }
        .modal {
            width: 60vh !important;
            max-height: 120vh !important;
        }

        .modal-dialog{
            overflow-y: initial !important
        }
        .modal-body{
            height: 250px;
            overflow-y: auto;
        }

        #primal_style{
            filter: blur(2px);
        }

        @media only screen and (max-width: 1200px) {
            .responsive-modal{
               height: 55vw !important;
            }
        }

        @media only screen and (min-width: 1200px){
            .responsive-modal{
                height: 40vw !important;
            }
        }
    </style>
</head>
<body>





