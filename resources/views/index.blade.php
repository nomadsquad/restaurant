@include('includes/header')

<meta name="csrf-token" content="{{ csrf_token() }}" />

<input id="user_id" type="hidden" value="1">
<div id="primal_style">
    <div class="row">
        <div class="col s12 m6">
            <div class="card hoverable animated bounceInLeft">
                <div class="card-image">
                    <div class="carousel carousel-slider" id="product_images" data-wrap="true">
                        <a class="carousel-item" href="#"><img src="https://lorempixel.com/800/400/food/1"></a>
                    </div>
                    <span class="card-title white-text" id="product_title"></span>
                    <a
                            class="btn-floating halfway-fab waves-effect waves-light red dropdown-button"
                            href="#"
                            id="collapse_info"
                    >
                        <i class="material-icons">help</i>
                    </a>
                </div>
                <div class="card-content" id="product_content"></div>
                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header" style="display: none;"><i class="material-icons">place</i>Additional info</div>
                        <div class="collapsible-body">
                            <div class="left"><b>Price: </b><span id="product_price"></span> RON</div>
                            <div class="right"><b>On stock: </b><span id="product_stock"></span></div>
                            <table class="responsive-table">
                                <thead>
                                <tr>
                                    <th>Calories</th>
                                    <th>Proteins</th>
                                    <th>Carbohydrates</th>
                                    <th>Vitamins</th>
                                    <th>Lipids</th>
                                </tr>
                                </thead>

                                <tbody id="product_info">

                                </tbody>
                            </table>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col s12 m6">
            <div class="card hoverable animated zoomIn">
                <div class="card-panel">
                    <div class="center-align">
                        <span class="card-title black-text">Order List</span>

                        <table class="highlight responsive-table" id="order_list">
                            <thead>
                            <tr>
                                <th>Item Name</th>
                                <th>Item Price</th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody id="order_item">

                            </tbody>
                        </table>
                    </div>
                    <br>
                    <button class="btn btn-info" id="send_order">Send Order</button>
                    <div class="right"><b>Total Price: <span id="total_price">0</span> RON</b></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col s12 m6">
            <div class="card animated fadeInDownBig">
                <div class="card-content">
                    <h3 style="font-family: Raleway">Current selection: <span id="course" data-id=""></span></h3>
                    <button id="previous_course" class="btn btn-danger">Previous</button>
                    <button id="next_course" class="btn btn-danger">Next</button>
                </div>
            </div>
        </div>

        <div class="col s12 m6">
            <div class="card animated fadeInDownBig green">
                <div class="card-content">
                    <h3 style="font-family: Raleway">Chef recommendation</h3>
                    <div id="recommendation">
                        <span><b>Course: </b> First Course</span>
                        <br>
                        <span><b>Item: </b>Pollo Parmigiano and White Wine</span>
                        <br>
                        <span><b>Price: </b> 32 lei</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Structure -->
<div id="login_modal" class="modal modal-fixed-footer" style="overflow-y: hidden; height: 81vh !important;">
    <div class="modal-dialog">
        <div class="modal-header center-align blue">
            <br>
            <img class="responsive-img white-text" style="height: 15vh;" src="storage/men-icon.png">
            <br>
            <br>
            <h4 class="white-text" style=" font-family: Oswald;">User Authentication</h4>
            <p></p>
            <br>
        </div>

        <div id="login_phase" class="modal-content">
            <div class="row">
                <div class="container">
                    <div class="input-field">
                        <i class="material-icons prefix">account_circle</i>
                        <input id="username_login" type="text" class="validate">
                        <label for="username_login">Username</label>
                    </div>

                    <br>

                    <div class="input-field">
                        <i class="material-icons prefix">vpn_key</i>
                        <input id="password_login" type="password" class="validate">
                        <label for="password_login">Password</label>
                    </div>

                    <section style="height: 80px"></section>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <div class="right">
                <button type="button" class="modal-action btn blue lighter-1 modal-close waves-effect waves-green btn-flat" id="login_user">Sign In</button>
            </div>
            <div class="left">
                <button type="button" class="modal-action btn red lighter-2 modal-close waves-effect waves-green btn-flat" id="register_form">Register</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Structure -->
<div id="register_modal" class="modal modal-fixed-footer" style="overflow-y: hidden; height: 80vh !important;">
    <div class="modal-dialog">
        <div class="modal-header center-align blue">
            <br>
            <img class="responsive-img white-text" style="height: 15vh;" src="storage/men-icon.png">
            <br>
            <br>
            <h4 class="white-text" style=" font-family: Oswald;">Register User</h4>
            <p></p>
            <br>
        </div>

        <div id="register_phase" class="modal-content">
            <div class="row">
                <div class="container">
                    <div class="input-field">
                        <i class="material-icons prefix">account_circle</i>
                        <input id="username_register" type="text" class="validate">
                        <label for="username_register">Username</label>
                    </div>

                    <br>

                    <div class="input-field">
                        <i class="material-icons prefix">contacts</i>
                        <input id="name_register" type="text" class="validate">
                        <label for="name_register">Full name</label>
                    </div>

                    <br>

                    <div class="input-field">
                        <i class="material-icons prefix">vpn_key</i>
                        <input id="password_register" type="password" class="validate">
                        <label for="password_register">Password</label>
                    </div>

                    <br>

                    <div class="input-field">
                        <i class="material-icons prefix">autorenew</i>
                        <input id="repeat_password_register" type="password" class="validate">
                        <label for="repeat_password_register">Repeat Password</label>
                    </div>

                    <br>

                    <div class="input-field">
                        <i class="material-icons prefix">email</i>
                        <input id="email_register" type="email" class="validate">
                        <label for="email_register">Email</label>
                    </div>
                </div>
                <section style="height: 240px"></section>
            </div>
        </div>

        <div class="modal-footer">
            <div class="right">
                <button type="button" class="modal-action btn red lighter-2 modal-close waves-effect waves-green btn-flat" id="register_user">Register</button>
            </div>
            <div class="left">
                <button type="button" class="modal-action btn blue-grey lighten-5 modal-close waves-effect waves-green btn-flat" id="back_to_login">Back</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal Structure -->
<div id="post_order" class="responsive-modal modal modal-fixed-footer blue" style="overflow-y: hidden; width: 95vw !important;">
    <div class="modal-dialog">
        <div class="modal-header center-align">

        </div>

        <div class="modal-content" style="overflow: hidden">
            <div class="row">
                <div class="container" style="font-family: 'Ubuntu Condensed', sans-serif; font-size: large; color: white;">
                    <div class="col s12 m4 l4" style="text-align: center;">
                        <div>
                            <img class="deactivate-image responsive-img" src="/storage/one.png">
                        </div>
                        <div class="center-align">
                            Order delivered with success!
                            <br>
                            Please wait will the chef will prepare your order!
                        </div>
                    </div>
                    <div class="col s12 m4 l4" style="text-align: center;">
                        <div>
                            <img class="active-image responsive-img" src="/storage/two.png">
                        </div>
                        <div class="center-align">
                            <p>Wonderful! <br> The chef is now preparing your order!</p>
                            We would like to thank you for your patience!
                            <br>
                            Your order will be out in a second!
                        </div>
                    </div>
                    <div class="col s12 m4 l4" style="text-align: center;">
                        <div>
                            <img class="deactivate-image responsive-img" src="/storage/three.png">
                        </div>
                        <div class="center-align">
                            Your order is ready now!
                            <br>
                            In a couple of seconds you will receive our chef creation!
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <div class="left">
                {{--<button type="button" class="modal-action btn red lighter-2 modal-close waves-effect waves-green btn-flat" id="register_user">Register</button>--}}
            </div>
        </div>
    </div>
</div>

<div class="fixed-action-btn">
    <!-- Element Showed -->
    <a id="show_help" class="waves-effect waves-light btn btn-floating blue"><i class="material-icons">help</i></a>

    <!-- Tap Target Structure -->
    <div class="tap-target blue lighten-1 right-align" data-activates="show_help">
        <div class="tap-target-content">
            <h5>You are not logged!</h5>
            <p>
                Please login in order to continue browsing.
                <br>
                If you don't have an account feel free to create your account!
            </p>
        </div>
    </div>
</div>

@include('includes/footer')