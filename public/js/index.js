var courses = {};
var products = {};
var itemsIdArray = [];
var currentListItemId = 0;
var totalPrice = 0;

$(document).ready(function(){

    // Toolkit's and components configuration
    $('.modal').modal({
        dismissible: false,
        clickClose: false,
        showClose: false
    });
    // $("#post_order").modal("open");

    $('.tap-target').tapTarget('open');
    $('.carousel.carousel-slider').carousel({
        fullWidth: true
    });
    $('.collapsible').collapsible({
        accordion: true
    });

    // Components functionality
    $('#show_help').click(function () {
        $('.tap-target').tapTarget('open');
    });

    $('html').one('click',function (){
        $('#login_modal').modal('open');
    });

    $('#register_form').on("click", function (e) {
        $('#login_modal').modal('close');
        $('#register_modal').modal('open');

        e.preventDefault();
        e.stopImmediatePropagation();
    });

    $('#back_to_login').on("click", function () {
        $('#register_modal').modal('close');
        $('#login_modal').modal('open');
    });

    $("#next_course").click(function () {
        var currentItem = $("#course").attr("data-id");
        nextCourse(1, currentItem);
    });

    $("#previous_course").click(function () {
        var currentItem = $("#course").attr("data-id");
        nextCourse(0, currentItem);
    });

    $(document).on("click", "#next_product", function () {
        nextProduct(1);
    });

    $(document).on("click", "#previous_product", function () {
        nextProduct(0);
    });

    $("#collapse_info").click(function(){
        $('.collapsible').collapsible('open', 0);
    });

    // Application main login and functionality

    // Login User
    $('#login_user').on("click", function (e) {
        $.ajax({
            url: '/login',
            type: "POST",
            data: {
                "_token": $("#token").val(),
                username: $("#username_login").val(),
                password: $("#password_login").val()
            },
            success: function(data){
                $('#login_modal').modal('close');
                $('#user_id').val(data);
                $('#primal_style').removeAttr("style");
                $('#show_help').hide();
            },
            error: function (err) {
                Materialize.toast(err.responseJSON, 2000);
            }

        });

        e.preventDefault();
        e.stopImmediatePropagation();
    });

    // Register User
    $('#register_user').on("click", function(e){
        var username = $("#username_register").val();
        var password = $("#password_register").val();
        var repeatPassword = $("#repeat_password_register").val();
        var email = $("#email_register").val();
        var name = $("#name_register").val();

        var validate = validateRegister(username, password, repeatPassword, email, name);

        if(validate === true){
            $.ajax({
                url: '/register',
                type: "POST",
                data: {
                    "_token": $("#token").val(),
                    username: username,
                    password: password,
                    email: email,
                    name: name
                },
                success: function(data){
                    $('#register_modal').modal('close');
                    $('#user_id').val(data);
                    $('#primal_style').removeAttr("style");
                    $('#show_help').hide();
                },
                error: function (err) {
                    Materialize.toast(err.responseJSON, 2000);
                }
            });
        }

        e.preventDefault();
        e.stopImmediatePropagation();
    });

    //Get All Courses
    $.ajax({
        url: '/getCourses',
        type: "GET",
        success: function(data) {
            courses = data;
            $("#course").attr("data-id", data[0].id - 1);
            $('#course').append(data[0].type);
            getNewProducts(data[0].id);
        }
    });

    //Add item to cart
    $(document).on("click", "#add_to_cart", function () {
        var selectedId = $(".carousel-item.active").attr("data-id");
        var selectedName = $(".carousel-item.active").attr("data-name");
        var selectedPrice = $(".carousel-item.active").attr("data-price");
        totalPrice = 0;

        if(itemsIdArray === []){
            currentListItemId = 0;
        } else {
            currentListItemId++;
        }

        itemsIdArray.push(selectedId);

        $("#order_item").append("<tr><td>" + selectedName + "</td><td>" + selectedPrice + " RON</td><td><i onclick='removeItem(" + currentListItemId + ")' data-item='" + selectedId + "' data-id='" + currentListItemId + "' data-price='" + selectedPrice + "' class='material-icons item' style='cursor: pointer;'>delete_forever</i></td></tr>")

        $(".item").each(function (index, value) {
            var currentPrice = parseInt($(value).attr("data-price"));
            totalPrice = totalPrice + currentPrice
        });

        if($(".item").length == 1){
            $("#total_price").html($(".item").attr("data-price"));
        } else {
            $("#total_price").html(totalPrice);
        }
    });

    //Send order to kitchen
    $("#send_order").click(function () {
        var itemsArray = [];
        var clientId = $("#user_id").val();

        $(".item").each(function (index, value) {
            itemsArray.push($(value).attr("data-item"));
        });

        if(itemsArray.length > 0){
            //Post Order
            $.ajax({
                url: '/postOrder',
                type: "POST",
                data: {
                    items_array: itemsArray,
                    client_id: clientId
                },
                success: function() {
                    $("#post_order").modal("open");
                }
            });
        } else {
            Materialize.toast("Order list is empty!", 2000)
        }
    })

});

//App functions

//Validate register form
function validateRegister(username, password, repeatPassword, email, name){
    var regExEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

    if(username !== "" && password !== "" && repeatPassword !== "" && email !== "" && name !== ""){
        if(password !== repeatPassword){
            Materialize.toast("Passwords doesn't match!", 2000);
            return false;
        }

        if(!regExEmail.test(email)){
            Materialize.toast("Invalid email format!", 2000);
            return false;
        }

        return true;
    } else {
        Materialize.toast("Please fill all of the fields!", 2000);
        return false;
    }
}

//Previous or Next Course functionality
function nextCourse(type, id){
    var selectedId = "";
    var parsedId = parseInt(id);

    if(parsedId + 1 === 5 && type === 1) {
        selectedId = 0;
    } else if(parsedId - 1 === -1 && type === 0) {
        selectedId = 4;
    } else {
        if (type === 1) {
            selectedId = parsedId + 1;

        } else {
            selectedId = parsedId - 1;
        }
    }

    getNewProducts(courses[selectedId].id);
    $("#course").html("");
    $("#course").attr("data-id", courses[selectedId].id - 1);
    $('#course').append(courses[selectedId].type);
}

//Previous or Next Product functionality
function nextProduct(type){
    if(type === 1){
        $('.carousel').carousel("next");
        setTimeout(function () {
            var next_item = parseInt($(".carousel-item.active").attr("data-item"));

            $("#product_title").html(products[next_item].name);
            $("#product_content").html("<p>" + products[next_item].details + "</p><br><button id='add_to_cart' class='btn btn-danger'>Add to cart</button>");
            $("#product_price").html(products[next_item].price);
            $("#product_stock").html(products[next_item].stock);
            $("#product_info").html("<tr><td>"+ products[next_item].calories +"</td><td>"+products[next_item].proteins+"</td><td>"+products[next_item].carbohydrates+"</td><td>"+products[next_item].vitamins+"</td><td>"+products[next_item].lipids+"</td></tr>");

        }, 300);
    } else {
        $('.carousel').carousel("prev");
        setTimeout(function () {

            var prev_item = parseInt($(".carousel-item.active").attr("data-item"));

            $("#product_title").html(products[prev_item].name);
            $("#product_content").html("<p>" + products[prev_item].details + "</p><br><button id='add_to_cart' class='btn btn-danger'>Add to cart</button>");
            $("#product_price").html(products[prev_item].price);
            $("#product_stock").html(products[prev_item].stock);
            $("#product_info").html("<tr><td>"+ products[prev_item].calories +"</td><td>"+products[prev_item].proteins+"</td><td>"+products[prev_item].carbohydrates+"</td><td>"+products[prev_item].vitamins+"</td><td>"+products[prev_item].lipids+"</td></tr>");

        }, 300);
    }

}

//Refresh products data when course is modified
//Obtain all products from the current course and show it
function getNewProducts(id){
    $.ajax({
        url: '/getProducts',
        type: "POST",
        data: {
            "_token": $("#token").val(),
            course_id: id
        },
        success: function(data){
            products = data;

            $("#product_images").html("<div class=\"carousel-fixed-item\"><img id='next_product' class='icon-left-size' src='storage/arrow_nigger.svg'>" +
                "<img id='previous_product' class='icon-right-size' src='storage/arrow_nigger.svg'></div>");
            $(data).each(function (index, value) {
                var parsedId = parseInt(value.id) - 1;

                $("#product_images").append("<a class='carousel-item' id='add_item' data-id='"+ value.id +"' data-name='"+ value.name +"' data-price='" + value.price + "' href='#"+ parsedId +"' data-item=" + index + "><img class='image-size' src='storage/" + value.image + "'></a>");
                if ($('.carousel.carousel-slider').hasClass('initialized')){
                    $('.carousel.carousel-slider').removeClass('initialized')
                }
                $('.carousel.carousel-slider').carousel({fullWidth: true});
            });

            $("#product_title").html(data[0].name);
            $("#product_content").html("<p>" + data[0].details + "</p><br><button id='add_to_cart' class='btn btn-danger'>Add to cart</button>");
            $("#product_price").html(data[0].price);
            $("#product_stock").html(data[0].stock);
            $("#product_info").html("<tr><td>"+ data[0].calories +"</td><td>"+data[0].proteins+"</td><td>"+data[0].carbohydrates+"</td><td>"+data[0].vitamins+"</td><td>"+data[0].lipids+"</td></tr>");
        }
    });
}

//Remove item from cart
function removeItem(itemId) {
    $(".item").each(function (index, value) {
        if(itemId === parseInt($(value).attr("data-id"))){
            var currentPrice = parseInt($(value).attr("data-price"));
            $(value).parent().parent().remove();
            totalPrice = totalPrice - currentPrice
        }
    });


    if(!$(".item").length){
        $("#total_price").html(0);
        totalPrice = 0;
    } else {
        $("#total_price").html(totalPrice);
    }
}