<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use Mockery\Exception;
use PhpParser\Error;
use Psy\Exception\ErrorException;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');
        $email = $request->input('email');
        $name = $request->input('name');

        $validate_username = Client::where("username", $username)->first();
        $validate_email = Client::where("email", $email)->first();

        if($validate_username != ""){
            return response()->json("Username already exists!", 401);
        } elseif ($validate_email != ""){
            return response()->json("Email already exists!", 401);
        }

        $client = new Client();
        $client->username = $username;
        $client->password = $password;
        $client->email = $email;
        $client->name = $name;
        $client->save();
        $findNewClient = Client::where("username", $username) ->first();

        return $findNewClient->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        //
    }

    public function login(Request $request){

        $username = $request->input("username");
        $password = $request->input("password");

        $query = Client::where("username", $username)->where("password", $password)->first();

        if($query != ""){
            return $query->id;
        } else {
            return response()->json("Login failed! Please verify your login information!", 401);
        }
    }
}
