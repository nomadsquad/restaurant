<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::post('/login', "ClientController@login");

Route::post('/register', "ClientController@store");

Route::post('/getProducts', 'ProductController@getProducts');

Route::get('/getCourses', "CourseController@getCourses");

Route::post('/postOrder', "OrderController@postOrder");