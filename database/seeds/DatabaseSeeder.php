<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//         $this->call(UsersTableSeeder::class);

        DB::table('clients')->insert([
            'username' => 'test_client',
            'name' => 'John Doe',
            'email' => 'johndoe@email.com',
            'password' => '123456'
        ]);

        DB::table('staff')->insert([
            'username' => 'admin',
            'name' => 'Administrator',
            'email' => 'admin@email.com',
            'password' => '123456'
        ]);

        DB::table('products')->insert([
            'name' => 'Burger 1',
            'image' => '1.jpg',
            'details' => "Delicious Burger",
            'price' => '17',
            'stock' => '5',
            'calories' => '1600',
            'proteins' => '30',
            'carbohydrates' => '65',
            'vitamins' => '14',
            'lipids' => '97'
        ]);

        DB::table('products')->insert([
            'name' => 'Burger 2',
            'image' => '2.jpg',
            'details' => "Delicious Burger Yum yum",
            'price' => '17',
            'stock' => '5',
            'calories' => '1600',
            'proteins' => '30',
            'carbohydrates' => '65',
            'vitamins' => '14',
            'lipids' => '97'
        ]);

        DB::table('products')->insert([
            'name' => 'Burger 3',
            'image' => '3.jpeg',
            'details' => "Not so delicious but still worth it",
            'price' => '17',
            'stock' => '5',
            'calories' => '1600',
            'proteins' => '30',
            'carbohydrates' => '65',
            'vitamins' => '14',
            'lipids' => '97'
        ]);

        DB::table('products')->insert([
            'name' => 'Pizza',
            'image' => '4.jpg',
            'details' => "Dope Pizza",
            'price' => '17',
            'stock' => '5',
            'calories' => '1600',
            'proteins' => '30',
            'carbohydrates' => '65',
            'vitamins' => '14',
            'lipids' => '97'
        ]);

        DB::table('products')->insert([
            'name' => 'Pasta',
            'image' => '5.jpg',
            'details' => "That Pasta! Yummy!",
            'price' => '17',
            'stock' => '5',
            'calories' => '1600',
            'proteins' => '30',
            'carbohydrates' => '65',
            'vitamins' => '14',
            'lipids' => '97'
        ]);

        DB::table('orders')->insert([
            'id_client' => '1',
            'id_product' => '1',
            'status' => '2',
        ]);

        DB::table('courses')->insert([
            'type' => 'Appetizer',
        ]);

        DB::table('courses')->insert([
            'type' => 'First Course',
        ]);

        DB::table('courses')->insert([
            'type' => 'Main Course',
        ]);

        DB::table('courses')->insert([
            'type' => 'Dessert',
        ]);

        DB::table('courses')->insert([
            'type' => 'Beverage',
        ]);

        DB::table('products_courses')->insert([
            'product_id' => '1',
            'course_id' => '1'
        ]);
        DB::table('products_courses')->insert([
            'product_id' => '2',
            'course_id' => '1'
        ]);
        DB::table('products_courses')->insert([
            'product_id' => '3',
            'course_id' => '1'
        ]);
        DB::table('products_courses')->insert([
            'product_id' => '4',
            'course_id' => '2'
        ]);
        DB::table('products_courses')->insert([
            'product_id' => '5',
            'course_id' => '2'
        ]);
    }
}
